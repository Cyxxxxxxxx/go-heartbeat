package udp

import (
	"WebSocket/redis"
	"encoding/json"
	"fmt"
	"net"
)

// UDP 客户端
func UDPClient(port int, User redis.UserStatus) *redis.UserStatus {
	socket, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   net.IPv4(0, 0, 0, 0),
		Port: port,
	})
	defer func() {
		if err := socket.Close(); err != nil {
			fmt.Println("socket close err")
		}
	}()
	if err != nil {
		fmt.Println("连接对应UDP服务器件失败,err:", err.Error())
		return nil
	}

	//send data
	sendData, _ := json.Marshal(User)
	_, err = socket.Write(sendData) // 发送数据
	if err != nil {
		fmt.Println("发送数据失败,err:", err.Error())
		return nil
	}

	//recv data
	data := make([]byte, 4096)
	n, _, err := socket.ReadFromUDP(data) // 接收数据
	if err != nil {
		fmt.Println("接收数据失败,err:", err.Error())
		return nil
	}
	recvData := redis.UserStatus{}
	_ = json.Unmarshal(data[:n], &recvData)
	//fmt.Printf("recv:%s addr:%v count:%d\n", string(data[:n]), remoteAddr, n)
	return &recvData
}
