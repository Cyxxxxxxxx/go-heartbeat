package udp

import (
	"WebSocket/redis"
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"time"
)

// UDP server端
func UDPServer(port int) {
	listen, err := net.ListenUDP("udp", &net.UDPAddr{
		IP:   net.IPv4(0, 0, 0, 0),
		Port: port,
	})
	defer listen.Close()
	if err != nil {
		fmt.Println("listen failed, err:", err)
		return
	}

	for {
		var data [4096]byte
		n, addr, err := listen.ReadFromUDP(data[:]) // 接收数据
		if err != nil {
			fmt.Println("read udp failed, err:", err.Error())
			continue
		}

		//-------修改用户状态信息
		recvData := redis.UserStatus{}
		_ = json.Unmarshal(data[:n], &recvData)
		//fmt.Println("server 接受：", recvData)
		rand.Seed(time.Now().UnixNano())
		recvData.ServerStatus = uint8(rand.Intn(3)) //假设为一些可能的业务操作
		recvData.AppStatus = uint8(rand.Intn(3))
		recvData.Timestamp = float64(time.Now().Unix())
		sendData, _ := json.Marshal(recvData)
		//--------

		_, err = listen.WriteToUDP(sendData, addr) // 发送数据
		if err != nil {
			fmt.Println("write to udp failed, err:", err.Error())
			continue
		}
	}
}
