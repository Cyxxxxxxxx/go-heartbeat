package main

import (
	"WebSocket/redis"
	"WebSocket/service"
	"sync"
	"time"
)

func main() {
	/*go udp.UDPServer()
	var wg sync.WaitGroup
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go func() {
			udp.UDPClient(i)
			defer wg.Done()
		}()
	}

	wg.Wait()*/

	wg := sync.WaitGroup{}
	redis.Init()

	wg.Add(2)
	service.NewUser()
	time.Sleep(2)
	go func() {
		service.HBclient()
		wg.Done()
	}()
	go func() {
		service.HBserver()
		wg.Done()
	}()
	wg.Wait()

	//先到这吧 先睡了
	//结束 demo
	//todo 增加健壮性
}
