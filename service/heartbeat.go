package service

import (
	"WebSocket/redis"
	"WebSocket/udp"
	"fmt"
	"github.com/gookit/color"
	uuid "github.com/satori/go.uuid"
	"math/rand"
	"time"
)

//var UserMap = sync.Map{}

func GetRandTimeAdd() (timeNow float64) {
	rand.Seed(time.Now().UnixNano())
	switch rand.Intn(3) {
	case 0:
		timeNow = float64(time.Now().Add(5 * time.Second).Unix())
	case 1:
		timeNow = float64(time.Now().Add(8 * time.Second).Unix())
	case 2:
		timeNow = float64(time.Now().Add(10 * time.Second).Unix())
	case 3:
		timeNow = float64(time.Now().Add(20 * time.Second).Unix())
	}
	return
}

// 模拟没有频繁的数据往来  就加入延时队列
func NewUser() {
	redis.ZsetDelAll()
	for init := 0; init < 1; init++ {
		User := redis.UserStatus{}
		//假设长连接发送消息在30内没有消息来往  设置每随机秒udp一下客户端 保持用户状态
		User.Timestamp = GetRandTimeAdd() //获取随机增加的秒数
		User.UID = uuid.NewV4().String()
		//UserMap.Store(User.UID, User) //暂时本地存一下用户信息
		User.ZsetAdd() //加入redis的zset中
	}
}

// 服务器端的心跳模拟
// 1.从zset中取最小时间与系统当前时间比较 小于  则发一次心跳包给客户端UDP服务
// 2.客户端UDP服务接受并处理
// 3.接受到客户端UDP服务处理的结构 分析结果是否满足现在要求
func HBserver() {
	fmt.Println("Heartbeat Demo Start ...")
	redis.ZsetLen() //查看zset集合中的元素总数
	for {
		uid := redis.ZsetRangeOne()             //取到一个时间最小的元素
		uidTimestamp := redis.ZsetGetSocre(uid) //取timestamp
		if time.Now().Unix() >= int64(uidTimestamp) {
			user := redis.UserStatus{}
			user.UID = uid
			user.Timestamp = uidTimestamp
			j := 0
			for i := 0; i < 5; i++ {
				recvData := udp.UDPClient(20000, user)
				/*fmt.Println(recvData)
				fmt.Println(int64(user.Timestamp))
				fmt.Println(int64(recvData.Timestamp))*/
				if recvData.AppStatus != uint8(0) && recvData.AppStatus != uint8(0) { //预设的两个状态都不为0 才认定状态正常
					color.Green.Println("接收到client返回的正常心跳状态(1-在线 0-离线)   UID:", recvData.UID, " Server状态->", recvData.ServerStatus, " App状态->", recvData.AppStatus, " 当前时间:", time.Now())
					recvData.Timestamp = GetRandTimeAdd() //获取随机增加的时间
					recvData.ZsetAdd()
					break
				}
				j++
			}
			if j == 5 { //5次状态不对 判定离线
				color.Red.Println("接收到client返回的正常心跳状态(1-在线 0-离线)全状态-0   UID:", user.UID, " 用户状态->离线", " 当前时间:", time.Now())
				/*//删除该用户
				//redis.ZsetDelOne(strconv.Itoa(int(user.Timestamp)))*/
				//采用重新设置时间 不删除
				user.Timestamp = GetRandTimeAdd()
				user.ZsetAdd()
			}
		}
	}
}

// 模拟客户端UDP服务
func HBclient() {
	udp.UDPServer(20000)
}
