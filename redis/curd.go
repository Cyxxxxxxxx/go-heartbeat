package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
)

type UserStatus struct {
	UID          string  `json:"uid"`
	AppStatus    uint8   `json:"app_status"`    //应用级别的用户状态 0-下线 1-在线
	ServerStatus uint8   `json:"server_status"` //服务级别的用户状态
	Timestamp    float64 `json:"timestamp"`
}

var (
	ZsetKey = "UserStatusInfos"
	ctx     = context.Background()
)

func ZsetDelOne(key string) {
	rdb.ZRemRangeByScore(ctx, ZsetKey, key, key)
}

func ZsetDelAll() {
	rdb.ZRemRangeByRank(ctx, ZsetKey, 0, 1000000)
}

func (U *UserStatus) ZsetAdd() {
	if err := rdb.ZAdd(ctx, ZsetKey, &redis.Z{Score: U.Timestamp, Member: U.UID}).Err(); err != nil {
		fmt.Println(err.Error())
	}
}

// 加大score
func (U *UserStatus) ZsetIncr(t float64) {
	rdb.ZIncrBy(ctx, ZsetKey, t, U.UID)
}

// 获取集合中第一个 score最小
func ZsetRangeOne() string {
	// 返回从0到-1位置的集合元素， 元素按分数从小到大排序
	// 0到-1代表则返回全部数据
	vals, err := rdb.ZRange(ctx, ZsetKey, 0, 1).Result()
	if err != nil {
		panic(err)
	}
	return GetSliceElem(vals)
}

// 只取一票
func GetSliceElem(s []string) string {
	for _, val := range s {
		return val
	}
	return ""
}

func ZsetGetSocre(elem string) float64 {
	// 查询集合元素zhangsan的分数
	score, err := rdb.ZScore(ctx, ZsetKey, elem).Result()
	if err != nil {
		panic(err)
	}
	return score
}

func ZsetLen() {
	count, err := rdb.ZCard(ctx, ZsetKey).Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("zset count: ", count)
}
